//
//  TripCell.swift
//  SmartBus
//
//  Created by admin on 27.03.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var tripName: UILabel!
    @IBOutlet weak var tripDateFrom: UILabel!
    @IBOutlet weak var tripPrice: UILabel!
    var tripId: Int!
    
}
